%{
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int yylex();
void yyerror(char *);

char *build_op(char op, char *l, char *r) {
  char *str = malloc(strlen(l) + strlen(r) + 6);
  sprintf(str, "(%c %s %s)", op, l, r);
  return str;
}

char *build_neg(char *n) {
  char *str = malloc(strlen(n) + 5);
  sprintf(str, "(~ %s)", n);
  return str;
}

char *build_fundef(char *name, char *param, char *body, char *rest) {
  char *str = malloc(strlen(name) + strlen(param) + strlen(body) + strlen(rest) + 31);
  sprintf(str, "(def %s 0 (seq (:= %s (func %s %s)) %s))", name, name, param, body, rest);
  return str;
}

char *build_funcall(char *id, char *arg) {
  char *str = malloc(strlen(id) + strlen(arg) + 9);
  sprintf(str, "(call %s %s)", id, arg);
  return str;
}

char *build_cond(char *cond, char *then, char *el) {
  char *str = malloc(strlen(cond) + strlen(then) + strlen(el) + 10);
  sprintf(str, "(if %s %s %s)", cond, then, el);
  return str;
}

char *copy(char *orig) {
  char *str = malloc(strlen(orig) + 1);
  strcpy(str, orig);
  return str;
}

%}

%union {
  char *val;
}

/* terminal symbols */
%token <val> NUM ID
%token ADD SUB MUL DIV
%token OPEN CLOSE
%token IF THEN ELSE
%token FUNC

/* nonterminal symbols */
%nterm <val> expr ident cond
%nterm <val> fundef funcall

/* precedence - from lower to higher */
%left ADD SUB
%left MUL DIV
%left NEG
%left IF THEN ELSE
%left FUNCALL
%left FUNDEF

%%

input:
  |    expr                         { puts($1); }
  ;

expr:  NUM                          { $$ = copy($1); }
  |    fundef                       { $$ = copy($1); }
  |    funcall                      { $$ = copy($1); }
  |    cond                         { $$ = copy($1); }
  |    expr ADD expr                { $$ = build_op('+', $1, $3); }
  |    expr SUB expr                { $$ = build_op('-', $1, $3); }
  |    expr MUL expr                { $$ = build_op('*', $1, $3); }
  |    expr DIV expr                { $$ = build_op('/', $1, $3); }
  |    SUB expr %prec NEG           { $$ = build_neg($2); }
  |    OPEN expr CLOSE              { $$ = copy($2); }
  ;

ident: ID                           { $$ = copy($1); }
  ;

fundef: FUNC ident ident expr expr %prec FUNDEF { $$ = build_fundef($2, $3, $4, $5); }
  ;

funcall: ident expr %prec FUNCALL   { $$ = build_funcall($1, $2); }
  ;

cond: IF expr THEN expr ELSE expr   { $$ = build_cond($2, $4, $6); }
  ;

%%

void yyerror(char *s) {
  fprintf(stderr,"Erro no Bison: %s\n",s);
}

int main() {
  yyparse();
  return 0;
}
