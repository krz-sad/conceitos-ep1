 #!/bin/bash

run_tests() {
  printf "input: "
  echo "$input"
  printf "s-exp: "
  echo "$input" | ./parser.o
  # printf "answr: "
  # echo "$input" | ./parser.o | ./ep.o
  echo
}

printf "Compiling binaries...\n\n"
make
echo

printf "Changing permission of compiled binaries..\n\n"
chmod 500 parser.o
chmod 500 ep.o

input=`cat t_closure`
echo Press ENTER to perform Closure tests...
read enter
run_tests

input=`cat t_recursion`
echo Press ENTER to perform Recursion tests...
read enter
run_tests
