%option noyywrap

%{
#include "parser.tab.h"
%}

%%

[0-9]*        { yylval.val = yytext; return NUM; }
if            { return IF; }
then          { return THEN; }
else          { return ELSE; }
function      { return FUNC; }
[a-zA-Z0-9]+  { yylval.val = yytext; return ID; }
[(]           { return OPEN; }
[)]           { return CLOSE; }
[+]           { return ADD; }
[-]           { return SUB; }
[*]           { return MUL; }
[/]           { return DIV; }
[ \t\n]+      ;
.             ;

%%
